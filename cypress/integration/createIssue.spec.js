const faker = require('faker')

describe('Create Issue', () => {
  const project = {
    name: faker.random.uuid(),
    description: faker.random.words(5)
  }

  const issue = {
    project: project.name,
    title: faker.random.uuid(),
    description: faker.random.words(3)
  }

  beforeEach(() => {
    cy.login()
    cy.createProjectViaApi(Cypress.env('ACCESS_TOKEN'), project.name)
  })

  it('successfully', () => {
    cy.createIssue(issue)

    cy.get('.issue-details')
      .should('contain', issue.title)
      .and('contain', issue.description)
  })
})
